#include "SerialTask.h"
#include "utility.h"
#include "Arduino.h"
#include "MsgService.h"

extern volatile StateG GState;

SerialTask::SerialTask(int baud){
   this->baud = baud; 
   MsgService.init();
}
 
void SerialTask::init(int period){
  Task::init(period);
    
}
  
void SerialTask::tick(){ 
   if (MsgService.isMsgAvailable()) {
      Msg* msg = MsgService.receiveMsg();  
      if (msg->getContent() == "on")
      {
        GState = B;
      }
       if (msg->getContent() == "off")
      {
        GState = A;
      }
    }
}
