#ifndef __SERVOTASK__
#define __SERVOTASK__

#include "Task.h"
#include "UltrasonicSensor.h"
#include "ServoMot.h"

class ServoTask: public Task {

public:

  ServoTask(int trigPin, int echoPin, int servoPin);  
  void init(int period);  
  void tick();

private:
  UltrasonicSensor* sensor;
  ServoMot* myservo;
};

#endif
