#include "utility.h"
#include "ServoTask.h"
#include "Scheduler.h"
#include "SerialTask.h"
#include "BlinkTask.h"


Scheduler sched;
volatile StateG GState;

void setup() {

  sched.init(10);
  
  Task* t0 = new ServoTask(5,6,7);
  t0->init(30);
  sched.addTask(t0);

  Task* t1 = new SerialTask(9600);
  t0->init(10);
  sched.addTask(t1);

  Task* t3 = new BlinkTask(13);
  t3->init(50);
  sched.addTask(t3);
  
  GState = A;
}

void loop() {
  
  sched.schedule();
  
  
}
