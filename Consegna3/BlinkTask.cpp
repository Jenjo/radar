#include "BlinkTask.h"
#include "utility.h"
#include "Arduino.h"
extern volatile StateG GState;

BlinkTask::BlinkTask(int pin){
  this->pin = pin;    
}
  
void BlinkTask::init(int period){
  Task::init(period);
  led = new Led(pin);  
}
  
void BlinkTask::tick(){
  if(GState == B){
    led->switchOn();
  }else if(GState == A){
    led->switchOff();  
  }
  
}
