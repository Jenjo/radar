#ifndef __SERVOMOT___
#define __SERVOMOT___

#define PWMPERIOD 20

class ServoMot{
  public:
    ServoMot(int pin);
    void act(int perc);
  private:
    int pin;
};

#endif
