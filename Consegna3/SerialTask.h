#ifndef __SERIALTASK__
#define __SERIALTASK__

#include "Task.h"

class SerialTask: public Task {
  int baud=9600;

public:

  SerialTask(int baud);
  SerialTask();  
  void init(int period);  
  void tick();
};

#endif

