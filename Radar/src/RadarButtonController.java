import msg.*;
import java.io.IOException;
import java.util.*;
import java.io.*;

import common.BasicController;
import devices.Button;
import devices.Light;

public class RadarButtonController extends BasicController {
	
	private Light OnLed;
	private Light DetectedLed;
	private Light TrackingLed;
	private Button OnBtn;
	private Button OffBtn;
	SerialCommChannel channel; 
	private File file;
	int f1 = 0;
	public RadarButtonController(Button OnBtn, Button OffBtn, Light OnLed, Light DetectedLed, Light TrackingLed, String port) throws Exception {
		this.OnBtn = OnBtn;
		this.OffBtn = OffBtn;
		this.OnLed = OnLed;
		this.DetectedLed = DetectedLed;
		this.TrackingLed = TrackingLed;
		this.channel = new SerialCommChannel(port,9600);
		this.file = Objects.requireNonNull(new File("log.txt"));

	}
	
	public void run(){
		try {
			while (true){	
			  if (OnBtn.isPressed()){
				  System.out.println("Accendi");
				  OnLed.switchOn();
				  TrackingLed.switchOff();
				  DetectedLed.switchOff();	
				  channel.sendMsg("on");
				  f1 = 1;
				
			  }
			  if (OffBtn.isPressed()){
				  OnLed.switchOff(); 
				  TrackingLed.switchOff();
				  DetectedLed.switchOff();	
				  channel.sendMsg("off");
				  f1 = 0;
			  }
			  
			//TrackingLed 
			String distanza;
			  if(f1 == 1 && (distanza = channel.receiveMsg()).length() > 3){
				TrackingLed.switchOn();	
				String[] arr = distanza.split("\\s+");
				if(arr[0].equals("oo")) {
					System.out.println("Rilevazioni: " + arr[1]);
				} else {

					Date date = new Date();
					String logging = "Time: "+ date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds() + " Object Tracked at angle: " + arr[1] + " Distance: " + arr[2];
					System.out.println(logging);
					try (PrintWriter p = new PrintWriter(new BufferedWriter(new FileWriter("log.txt", true)))) {
						p.println(logging);
						p.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}				
			  } 
			  else {
				TrackingLed.switchOff();	
			  }

			//Detected Led con stampa dell'angolo e del tempo
			String ciao;
			  if (f1 == 1 && (ciao = channel.receiveMsg()).length() <= 3){

				DetectedLed.switchOn();
				waitFor(10);
				DetectedLed.switchOff();
				Date date = new Date();
				String toFiling = "Time: "+ date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds() + " Object Detected at angle: " + ciao;
				System.out.println(toFiling);
				try (PrintWriter p = new PrintWriter(new BufferedWriter(new FileWriter("log.txt", true)))) {
					p.println(toFiling);
					p.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			  }






			  waitFor(50);
			}		
		} catch (InterruptedException ex){
			ex.printStackTrace();
		} catch (IOException ex){
			ex.printStackTrace();
		}
	}
}
