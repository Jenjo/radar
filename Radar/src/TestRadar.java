import msg.*;
import java.io.*;

import devices.*;

public class TestRadar {

	public static void main(String[] args) throws Exception {	
		
		System.out.println("Waiting Arduino for rebooting...");		
		Thread.sleep(4000);
		System.out.println("Ready.");		

		Light Onled = new devices.p4j_impl.Led(7);
		Light DetectedLed = new devices.p4j_impl.Led(2);
		Light TrackingLed = new devices.p4j_impl.Led(3);
		Button OnBtn = new devices.p4j_impl.Button(0);
		Button OffBtn = new devices.p4j_impl.Button(4);
		new RadarButtonController(OnBtn,OffBtn,Onled,DetectedLed,TrackingLed, args[0]).start();
	}

}
