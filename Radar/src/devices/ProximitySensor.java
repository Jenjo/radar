package devices;

public interface ProximitySensor {

	boolean isObjDetected();
	
	double getObjDistance();
	
}
